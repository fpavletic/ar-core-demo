﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore;

public class SantaCarController : MonoBehaviour
{
	public AugmentedImage Image;

	private Vector3 _oldPosition;

    // Start is called before the first frame update
    void Start()
    {
	    _oldPosition = transform.localPosition;
	    transform.localPosition +=  new Vector3(0, 5, -10);
		
    }

    // Update is called once per frame
    void Update()
    {
	    if (Image == null || Image.TrackingState != TrackingState.Tracking)
	    {
		    return;
	    }

	    transform.localPosition = Vector3.MoveTowards(transform.localPosition, Vector3.zero, Time.deltaTime);
    }
}
