//-----------------------------------------------------------------------
// <copyright file="AugmentedImageExampleController.cs" company="Google">
//
// Copyright 2018 Google Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// </copyright>
//-----------------------------------------------------------------------

namespace GoogleARCore.Examples.AugmentedImage
{
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using GoogleARCore;
    using UnityEngine;
    using UnityEngine.UI;

    /// <summary>
    /// Controller for AugmentedImage example.
    /// </summary>
    public class AugmentedImageExampleController : MonoBehaviour
    {
        /// <summary>
        /// A prefab for visualizing an AugmentedImage.
        /// </summary>
        public GameObject IndicatorPrefab;

        /// <summary>
        /// The overlay containing the fit to scan user guide.
        /// </summary>
        public GameObject FitToScanOverlay;

	    private GameObject _spawnedImageGameObject;
	    private bool _spawnedImage;
	    private GameObject _spawnedPlaneGameObject;
		private bool _spawnedPlane;

        /// <summary>
        /// The Unity Update method.
        /// </summary>
        public void Update()
        {
            // Exit the app when the 'back' button is pressed.
            if (Input.GetKey(KeyCode.Escape))
            {
                Application.Quit();
            }

            // Check that motion tracking is tracking.
            if (Session.Status != SessionStatus.Tracking)
            {
                return;
            }

			List<DetectedPlane> detectedPlanes = new List<DetectedPlane>();
			Session.GetTrackables<DetectedPlane>(detectedPlanes, TrackableQueryFilter.Updated);
	        foreach (DetectedPlane detectedPlane in detectedPlanes)
	        {
				if (detectedPlane.TrackingState == TrackingState.Tracking && !_spawnedPlane)
		        {
			        _spawnedPlaneGameObject = Instantiate(IndicatorPrefab, new Vector3(0, 0, 0), Quaternion.Euler(0, 0, 0));
			        _spawnedPlane = true;
		        }
			}

			// Get updated augmented images for this frame.

			List<AugmentedImage> augmentedImages = new List<AugmentedImage>();
			Session.GetTrackables<AugmentedImage>(augmentedImages, TrackableQueryFilter.Updated);

            // Create visualizers and anchors for updated augmented images that are tracking and do not previously
            // have a visualizer. Remove visualizers for stopped images.
            foreach (var image in augmentedImages)
            {
				if (image.TrackingState == TrackingState.Tracking && !_spawnedImage)
                {
	                // Create an anchor to ensure that ARCore keeps tracking this augmented image.
					Anchor anchor = image.CreateAnchor(image.CenterPose);
                    
					_spawnedImageGameObject = Instantiate(IndicatorPrefab, anchor.transform);
					_spawnedImage = true;
				}
            }

	        if ( _spawnedImage && _spawnedPlane) { 
				_spawnedPlaneGameObject.transform.position = Vector3.MoveTowards(_spawnedPlaneGameObject.transform.position,
		        _spawnedImageGameObject.transform.position, Time.deltaTime * 0.2f);
			}

			FitToScanOverlay.SetActive(!_spawnedImage);
        }
    }
}
